find . -type f -regextype posix-extended -not -regex '^\./(node_modules|(\.[^/]+)).*' -regex '.*\.(([cm]?js)|(d\.[cm]?ts))$' -not -regex '^\./_[^/]+\.[cm]js$' -not -regex '^\./eslint\.config\.mjs$' -delete
find . -type f -regextype posix-extended -not -regex '^\./(node_modules|(\.[^/]+)).*' -regex '^\./([^/]+/)+package\.json$'  -delete
find . -empty -type d -delete
rm -rf node_modules package-lock.json node_modules
npm update -D