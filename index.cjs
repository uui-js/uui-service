"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Service = void 0;
const object_map_1 = require("@uui/object-map");
const node_fsp = __importStar(require("node:fs/promises"));
const node_path = __importStar(require("node:path"));
class Service {
    static watch = false;
    static instances = new object_map_1.ObjectMap();
    static instancesArray = [];
    static onChange(message) {
        console.log(JSON.parse(message));
    }
    static async serviceListen(cls, ...args) {
        let map = this.instances.get(cls);
        if (!map) {
            map = new Map();
            this.instances.set(cls, map);
        }
        const _key = cls.key ? cls.key(...args) : 0;
        let instance = map.get(_key);
        if (!instance) {
            instance = new cls(...args);
            map.set(_key, instance);
            this.instancesArray.push(instance);
            if (instance.listen && typeof instance.listen === 'function')
                await instance.listen();
        }
        return instance;
    }
    static async loadApplication(path) {
        let stats = undefined;
        try {
            stats = await node_fsp.stat(path);
        }
        catch (e) {
            return;
        }
        if (stats && stats.isDirectory()) {
            const fileName = path;
            let resolvePath = undefined;
            try {
                resolvePath = node_path.resolve(fileName);
            }
            catch (e) {
                console.log(e);
            }
            if (resolvePath) {
                let _module = undefined;
                try {
                    _module = await Promise.resolve(`${resolvePath}`).then(s => __importStar(require(s)));
                }
                catch (e) {
                    console.log(e);
                }
                if (_module && _module.default)
                    await this.serviceListen(_module.default);
            }
        }
    }
    static get(cls, key) {
        const map = this.instances.get(cls);
        return map ? map.get(key) : undefined;
    }
    static async listen() {
        const exit = () => {
            process.exit();
        };
        process.on('SIGTERM', exit);
        process.on('SIGQUIT', exit);
        const argv = process.argv.slice(2);
        this.watch = argv.indexOf('--watch') !== -1;
        if (this.watch) {
            process.on('message', this.onChange);
        }
        await this.loadApplication(process.cwd());
    }
    services = [];
    constructor() { }
    async serviceListen(cls, ...args) {
        return Service.serviceListen(cls, ...args);
    }
    async listen() {
        return;
    }
    async close() {
        return;
    }
}
exports.Service = Service;
/* eslint-enable */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLGdEQUEyQztBQUUzQywyREFBNEM7QUFDNUMscURBQXNDO0FBTXRDLE1BQWEsT0FBTztJQUNYLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFBO0lBQ3BCLE1BQU0sQ0FBQyxTQUFTLEdBQStFLElBQUksc0JBQVMsRUFHakgsQ0FBQTtJQUNLLE1BQU0sQ0FBQyxjQUFjLEdBQWMsRUFBRSxDQUFBO0lBQ3JDLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBZTtRQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQTtJQUNqQyxDQUFDO0lBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQXdDLEdBQWdCLEVBQUUsR0FBRyxJQUE4QjtRQUNwSCxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNqQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDVixHQUFHLEdBQUcsSUFBSSxHQUFHLEVBQXFDLENBQUE7WUFDbEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBQzdCLENBQUM7UUFDRCxNQUFNLElBQUksR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUMzQyxJQUFJLFFBQVEsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQzVCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNmLFFBQVEsR0FBRyxJQUFJLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFBO1lBQzNCLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFFBQVMsQ0FBQyxDQUFBO1lBQ3hCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVMsQ0FBQyxDQUFBO1lBQ25DLElBQUksUUFBUSxDQUFDLE1BQU0sSUFBSSxPQUFPLFFBQVEsQ0FBQyxNQUFNLEtBQUssVUFBVTtnQkFBRSxNQUFNLFFBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtRQUN2RixDQUFDO1FBQ0QsT0FBTyxRQUFRLENBQUE7SUFDaEIsQ0FBQztJQUNPLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLElBQVk7UUFDaEQsSUFBSSxLQUFLLEdBQThCLFNBQVMsQ0FBQTtRQUNoRCxJQUFJLENBQUM7WUFDSixLQUFLLEdBQUcsTUFBTSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1FBQ2xDLENBQUM7UUFBQyxPQUFPLENBQUMsRUFBRSxDQUFDO1lBQ1osT0FBTTtRQUNQLENBQUM7UUFDRCxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQztZQUNsQyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUE7WUFDckIsSUFBSSxXQUFXLEdBQXVCLFNBQVMsQ0FBQTtZQUMvQyxJQUFJLENBQUM7Z0JBQ0osV0FBVyxHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUE7WUFDMUMsQ0FBQztZQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7Z0JBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUNmLENBQUM7WUFDRCxJQUFJLFdBQVcsRUFBRSxDQUFDO2dCQUNqQixJQUFJLE9BQU8sR0FBUSxTQUFTLENBQUE7Z0JBQzVCLElBQUksQ0FBQztvQkFDSixPQUFPLEdBQUcseUJBQWEsV0FBVyx1Q0FBQyxDQUFBO2dCQUNwQyxDQUFDO2dCQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7b0JBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDZixDQUFDO2dCQUNELElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxPQUFPO29CQUFFLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDMUUsQ0FBQztRQUNGLENBQUM7SUFDRixDQUFDO0lBQ0QsTUFBTSxDQUFDLEdBQUcsQ0FBd0MsR0FBa0IsRUFBRSxHQUE2QjtRQUNsRyxNQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNuQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFBO0lBQ3RDLENBQUM7SUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU07UUFDbEIsTUFBTSxJQUFJLEdBQUcsR0FBRyxFQUFFO1lBQ2pCLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtRQUNmLENBQUMsQ0FBQTtRQUNELE9BQU8sQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQzNCLE9BQU8sQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFBO1FBQzNCLE1BQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQ2xDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtRQUMzQyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNoQixPQUFPLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7UUFDckMsQ0FBQztRQUNELE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQTtJQUMxQyxDQUFDO0lBQ08sUUFBUSxHQUFjLEVBQUUsQ0FBQTtJQUNoQyxnQkFBZSxDQUFDO0lBQ2hCLEtBQUssQ0FBQyxhQUFhLENBQXdDLEdBQWdCLEVBQUUsR0FBRyxJQUE4QjtRQUM3RyxPQUFPLE9BQU8sQ0FBQyxhQUFhLENBQUksR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLENBQUE7SUFDOUMsQ0FBQztJQUNELEtBQUssQ0FBQyxNQUFNO1FBQ1gsT0FBTTtJQUNQLENBQUM7SUFDRCxLQUFLLENBQUMsS0FBSztRQUNWLE9BQU07SUFDUCxDQUFDOztBQS9FRiwwQkFnRkM7QUFDRCxtQkFBbUIifQ==