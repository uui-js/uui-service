export default {
	semi: false,
	trailingComma: 'none',
	jsxSingleQuote: false,
	printWidth: 150,
	tabWidth: 4,
	useTabs: true,
	singleQuote: true,
	semi: false,
	quoteProps: 'preserve',
	bracketSpacing: true,
	bracketSameLine: false
}
