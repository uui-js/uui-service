export type CClass<T extends abstract new (...args: any[]) => InstanceType<T>> = new (...args: ConstructorParameters<T>) => InstanceType<T>;
export interface CService<T extends abstract new (...args: any[]) => InstanceType<T>> extends CClass<T> {
    key?: (...args: ConstructorParameters<T>) => number | string | symbol;
}
export declare class Service {
    private static watch;
    private static instances;
    private static instancesArray;
    private static onChange;
    static serviceListen<T extends new (...args: any[]) => any>(cls: CService<T>, ...args: ConstructorParameters<T>): Promise<InstanceType<T>>;
    private static loadApplication;
    static get<T extends new (...args: any[]) => any>(cls: CService<any>, key: string | symbol | number): InstanceType<T> | undefined;
    static listen(): Promise<void>;
    private services;
    constructor();
    serviceListen<T extends new (...args: any[]) => any>(cls: CService<T>, ...args: ConstructorParameters<T>): Promise<InstanceType<T>>;
    listen(): Promise<void>;
    close(): Promise<void>;
}
