bash clean.sh
npx eslint . --fix
npx prettier --write "./**/*.{js,jsx,mjs,cjs,ts,tsx,json}" 
find . -type f -regextype posix-extended -not -regex '^\./(node_modules|(\.[^/]+)).*' -regex '.*\.(([cm]?js)|(d\.[cm]?ts))$' -not -regex '^\./_[^/]+\.[cm]js$' -not -regex '^\./eslint\.config\.mjs$' -delete
find . -type f -regextype posix-extended -not -regex '^\./(node_modules|(\.[^/]+)).*' -regex '^\./([^/]+/)+package\.json$'  -delete
find . -empty -type d -delete
if [ -f tsconfig-types.json ]; then
npx tsc -p tsconfig-types.json
find . -type f -regextype posix-extended -regex '^\./([^/]+/)+index.\d\.ts$' -not -regex '^\./(node_modules|(\.[^/]+)).*' -exec bash -c 'cp _package-types.json $(dirname {})/package.json' \;
fi
if [ -f tsconfig-nojsx-node-cjs.json ]; then
npx tsc -p tsconfig-nojsx-node-cjs.json
find . -type f -regex '.*\.js$' -not -regex '\./\..*' -not -regex '^\./node_modules/.*' -exec rename 's/(.*)\.js$/$1\.cjs/' '{}' \;
fi
if [ -f tsconfig-nojsx-node-mjs.json ]; then
npx tsc -p tsconfig-nojsx-node-mjs.json
find . -type f -regex '.*\.js$' -not -regex '\./\..*' -not -regex '^\./node_modules/.*' -exec rename 's/(.*)\.js$/$1\.mjs/' '{}' \;
fi
