import { ObjectMap } from '@uui/object-map'
import * as node_fs from 'node:fs'
import * as node_fsp from 'node:fs/promises'
import * as node_path from 'node:path'
/* eslint-disable */
export type CClass<T extends abstract new (...args: any[]) => InstanceType<T>> = new (...args: ConstructorParameters<T>) => InstanceType<T>
export interface CService<T extends abstract new (...args: any[]) => InstanceType<T>> extends CClass<T> {
	key?: (...args: ConstructorParameters<T>) => number | string | symbol
}
export class Service {
	private static watch = false
	private static instances: ObjectMap<CService<any>, Map<string | symbol | number, InstanceType<any>>> = new ObjectMap<
		CService<any>,
		Map<string | symbol | number, InstanceType<any>>
	>()
	private static instancesArray: Service[] = []
	private static onChange(message: string) {
		console.log(JSON.parse(message))
	}
	static async serviceListen<T extends new (...args: any[]) => any>(cls: CService<T>, ...args: ConstructorParameters<T>): Promise<InstanceType<T>> {
		let map = this.instances.get(cls)
		if (!map) {
			map = new Map<string | symbol | number, Service>()
			this.instances.set(cls, map)
		}
		const _key = cls.key ? cls.key(...args) : 0
		let instance = map.get(_key)
		if (!instance) {
			instance = new cls(...args)
			map.set(_key, instance!)
			this.instancesArray.push(instance!)
			if (instance.listen && typeof instance.listen === 'function') await instance!.listen()
		}
		return instance
	}
	private static async loadApplication(path: string) {
		let stats: node_fs.Stats | undefined = undefined
		try {
			stats = await node_fsp.stat(path)
		} catch (e) {
			return
		}
		if (stats && stats.isDirectory()) {
			const fileName = path
			let resolvePath: string | undefined = undefined
			try {
				resolvePath = node_path.resolve(fileName)
			} catch (e) {
				console.log(e)
			}
			if (resolvePath) {
				let _module: any = undefined
				try {
					_module = await import(resolvePath)
				} catch (e) {
					console.log(e)
				}
				if (_module && _module.default) await this.serviceListen(_module.default)
			}
		}
	}
	static get<T extends new (...args: any[]) => any>(cls: CService<any>, key: string | symbol | number): InstanceType<T> | undefined {
		const map = this.instances.get(cls)
		return map ? map.get(key) : undefined
	}
	static async listen(): Promise<void> {
		const exit = () => {
			process.exit()
		}
		process.on('SIGTERM', exit)
		process.on('SIGQUIT', exit)
		const argv = process.argv.slice(2)
		this.watch = argv.indexOf('--watch') !== -1
		if (this.watch) {
			process.on('message', this.onChange)
		}
		await this.loadApplication(process.cwd())
	}
	private services: Service[] = []
	constructor() {}
	async serviceListen<T extends new (...args: any[]) => any>(cls: CService<T>, ...args: ConstructorParameters<T>) {
		return Service.serviceListen<T>(cls, ...args)
	}
	async listen(): Promise<void> {
		return
	}
	async close(): Promise<void> {
		return
	}
}
/* eslint-enable */
